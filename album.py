from flask import Flask, render_template, request, redirect, url_for, Response, session, abort, g
import os
import subprocess
import json
import datetime
import pytz
import threading
import time
from functools import wraps
import secrets

# Width and height for the frame the thumbnail will be fit inside.
THUMBNAIL_SIZE = 128

ACESS_TOKEN_LENGTH = 6

MIME_TYPES = {
    '.jpg': 'image/jpeg',
    '.mov': 'video/quicktime',
    '.mp4': 'video/mp4'
}

app = Flask(__name__)

with open('config.json', 'r') as f:
    config = json.loads(f.read())
    app.config['SECRET_KEY'] = config['secret_key']
app.config['DEBUG'] = True
app.config['TEMPLATES_AUTO_RELOAD'] = True

@app.template_filter('icon_for_filetype')
def icon_for_filetype(path):
    pathext = os.path.splitext(path)[1].lower()
    if pathext in FILETYPE_HANDLERS:
        return FILETYPE_HANDLERS[pathext]['icon']
    return '?'

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not 'logged_in' in session:
            return redirect(url_for('login', next=request.url))
        return f(*args, **kwargs)
    return decorated_function

def token_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        name, token = kwargs['name'], kwargs['token']
        album_data = get_album_meta(name)
        if album_data:
            if token in album_data['access_tokens']:
                return f(*args, **kwargs)
        abort(403)
    return decorated_function

def list_albums():
    with open('config.json', 'r') as f:
        config = json.loads(f.read())
        return config['albums']
    return None

def get_album_meta(name):
    with app.app_context():
        if getattr(g, 'meta_cache', None) is None:
            g.meta_cache = {}
        if name not in g.meta_cache:
            meta_dir = os.path.join('meta')
            meta_path = os.path.join(meta_dir, name + '.json')
            if os.path.exists(meta_path):
                with open(meta_path, 'r') as f:
                    g.meta_cache[name] = json.loads(f.read())
            else:
                return None
        return g.meta_cache[name]

def write_album_meta(name, data):
    with app.app_context():
        if not getattr(g, 'meta_cache', None) is None:
            g.meta_cache[name] = data
    meta_dir = os.path.join('meta')
    if not os.path.exists(meta_dir):
        os.makedirs(meta_dir)
    meta_path = os.path.join(meta_dir, name + '.json')
    with open(meta_path, 'w') as f:
        f.write(json.dumps(data, sort_keys=True, indent=4, separators=(',', ': ')))

def get_album_view_parameters(album):
    album_parameters = {'name': album}
    album_meta = get_album_meta(album)
    if album_meta:
        album_parameters['title'] = album_meta.get('title', album)
        album_parameters['timezone'] = album_meta.get('timezone', '')
        album_parameters['access_tokens'] = album_meta.get('access_tokens', {})
    return album_parameters

def contains_any(a, b):
    if not a or not b:
        return True
    for x in a:
        if a in b:
            return True
    return False

def generate_image_thumbnail(filepath, thumb_filepath):
    try:
        subprocess.run('magick convert -auto-orient "%s" -resize %dx%d "%s"' % (filepath, THUMBNAIL_SIZE, THUMBNAIL_SIZE, thumb_filepath), shell=True, check=True)
        return True
    except:
        pass
    return False

def generate_video_thumbnail(filepath, thumb_filepath):
    try:
        subprocess.run('ffmpeg -i "%s" -y -an -vf "thumbnail,scale=w=%d:h=%d:force_original_aspect_ratio=decrease" -frames:v 1 "%s"' % (filepath, THUMBNAIL_SIZE, THUMBNAIL_SIZE, thumb_filepath), shell=True, check=True)
        return True
    except:
        pass
    return False

def get_image_creation_time(filepath):
    try:
        process = subprocess.run('magick identify -format "%%[EXIF:DateTimeOriginal]" "%s"' % filepath, shell=True, check=True, stdout=subprocess.PIPE, encoding='latin1')
        time = datetime.datetime.strptime(process.stdout, '%Y:%m:%d %H:%M:%S')
        return time.timestamp()
    except:
        pass
    return None

def get_video_creation_time(filepath):
    try:
        process = subprocess.run('ffprobe -i "%s" -hide_banner' % filepath, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding='latin1')
        lines = process.stderr.splitlines()
        for line in lines:
            components = line.split()
            if components[0] == 'creation_time':
                time = datetime.datetime.strptime(components[-1], '%Y-%m-%dT%H:%M:%S.%fZ')
                return time.timestamp()
    except:
        pass
    return None

def generate_timestamps(path, album_meta):
    entries = album_meta['entries']
    tz = None
    try:
        if 'timezone' in album_meta and album_meta['timezone']:
            tz = pytz.timezone(album_meta['timezone'])
    except pytz.exceptions.UnknownTimeZoneError:
        return False
    entry = entries[path]
    try:
        if 'timezone' in entry and entry['timezone']:
            tz = pytz.timezone(entry['timezone'])
    except pytz.exceptions.UnknownTimeZoneError:
        return False
    d = datetime.datetime.fromtimestamp(entry['creation_time'], tz=tz)
    entry['timestamp'] = d.strftime('%Y-%m-%d %H:%M')
    group_timestamp = d.strftime('%Y-%m-%d')
    entry['group'] = group_timestamp
    return True

def layout_album(album_meta):
    entries = album_meta['entries']
    layout = list(entries.keys())
    layout.sort(key=lambda entry: entries[entry]['creation_time'])
    album_meta['layout'] = layout

IMAGE_HANDLER = {
    'icon': '\U0001F4F7',
    'thumbnail_generator': generate_image_thumbnail,
    'get_creation_time': get_image_creation_time
}

VIDEO_HANDLER = {
    'icon': '\U0001F4F9',
    'thumbnail_generator': generate_video_thumbnail,
    'get_creation_time': get_video_creation_time
}

FILETYPE_HANDLERS = {
    '.jpg': IMAGE_HANDLER,
    '.mov': VIDEO_HANDLER,
    '.mp4': VIDEO_HANDLER
}

background_tasks = []
background_tasks_revision = 0

class AlbumIndexer(threading.Thread):
    def __init__(self, name):
        super().__init__()
        self.name = name
        self.progress = 0
        # Determine what work needs to be done.
        self.album_meta = get_album_meta(self.name)
        if self.album_meta is None:
            self.album_meta = {
                'entries': {},
                'layout': [],
                'title': name,
                'timezone': '',
                'access_tokens': {}
            }
        # Index the album and generate metadata and thumbnails.
        albums = list_albums()
        self.album_dir = albums[self.name]
        self.thumb_dir = os.path.join('thumb', self.name)
        self.entries = self.album_meta['entries']
        self.added = []
        self.removed = []
        # Verify current entries, if they still exist or have been modified.
        for path in self.entries:
            full_filepath = os.path.join(self.album_dir, path)
            if os.path.exists(full_filepath):
                statbuf = os.stat(full_filepath)
                if statbuf.st_mtime > self.entries[path]['last_modified']:
                    self.added.append(path)
            else:
                self.removed.append(path)
        # Discover new files.
        for (root, dirnames, filenames) in os.walk(self.album_dir):
            filedir = os.path.relpath(root, self.album_dir)
            for filename in filenames:
                pathext = os.path.splitext(filename)[1].lower()
                rel_filepath = os.path.normpath(os.path.join(filedir, filename))
                path = rel_filepath.replace("\\","/")
                if pathext in FILETYPE_HANDLERS and path not in self.entries:
                    self.added.append(path)
        self.work_units = len(self.removed) + len(self.added)
        global background_tasks
        background_tasks.append(self)
        global background_tasks_revision
        background_tasks_revision += 1

    def run(self):
        global background_tasks_revision
        # Remove any missing files.
        for path in self.removed:
            thumb_filename = os.path.splitext(path)[0] + '.jpg'
            thumb_path = os.path.join(self.thumb_dir, thumb_filename)
            os.remove(thumb_path)
            del(self.entries[path])
            self.progress += 1
            background_tasks_revision += 1
        # Add new files or update modified files.
        for path in self.added:
            entry = {}
            if path in self.entries:
                entry = self.entries[path]
            full_filepath = os.path.join(self.album_dir, path)
            pathsplit = os.path.splitext(path)
            pathext = pathsplit[1].lower()
            thumb_filename = pathsplit[0] + '.jpg'
            thumb_path = os.path.join(self.thumb_dir, thumb_filename)
            if pathext in FILETYPE_HANDLERS:
                if not os.path.exists(os.path.dirname(thumb_path)):
                    os.makedirs(os.path.dirname(thumb_path))
                FILETYPE_HANDLERS[pathext]['thumbnail_generator'](full_filepath, thumb_path)
                statbuf = os.stat(full_filepath)
                entry['last_modified'] = statbuf.st_mtime
                creation_time = FILETYPE_HANDLERS[pathext]['get_creation_time'](full_filepath)
                if creation_time is None:
                    entry['creation_time'] = entry['last_modified']
                else:
                    entry['creation_time'] = creation_time
                entry['mime_type'] = MIME_TYPES[pathext]
                self.entries[path] = entry
                generate_timestamps(path, self.album_meta)
            self.progress += 1
            background_tasks_revision += 1
        layout_album(self.album_meta)
        write_album_meta(self.name, self.album_meta)
        global background_tasks
        background_tasks.remove(self)
        background_tasks_revision += 1

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        password = request.form['password']
        with open('config.json', 'r') as f:
            config = json.loads(f.read())
            if config['admin_password'] == password:
                session['logged_in'] = True
                return redirect(request.args.get('next'))
    return render_template('login.html')

# Album
def render_album(name, admin, token=None):
    message = 'Nothing'
    album_meta = get_album_meta(name)
    entries = album_meta['entries']
    last_group = None
    groups = []
    if token:
        tags = album_meta['access_tokens'][token]
        album_meta['layout'] = [path for path in album_meta['layout'] if contains_any(tags, entries[path].get('visibility_tags', None))]
    for path in album_meta['layout']:
        entry = entries[path]
        group_timestamp = entry['group']
        if last_group != group_timestamp:
            groups.append(group_timestamp)
            last_group = group_timestamp
    return render_template('album.html', message=message, name=name, admin=admin, token=token, album=album_meta, groups=groups)

@app.route('/album/<name>/<token>')
@token_required
def view_album(name, token):
    return render_album(name, False, token)

# Admin interface
@app.route('/admin/album/<name>', methods=['GET'])
@login_required
def view_admin_album(name):
    return render_album(name, True)

@app.route('/admin/album_entry', methods=['GET'])
@login_required
def view_admin_album_entry():
    name = request.args['name']
    path = request.args['path']
    album_meta = get_album_meta(name)
    entries = album_meta['entries']
    if path in entries:
        html = render_template('album_entry.html', name=name, admin=True, path=path, entry=entries[path])
        payload = {'entry': entries[path], 'html': html}
        return json.dumps(payload), 200, {'ContentType': 'application/json'} 
    abort(404)

@app.route('/admin/album/<name>', methods=['POST'])
@login_required
def modify_album_entry(name):
    path = request.form['path']
    album_meta = get_album_meta(name)
    payload = {'success': True}
    if path in album_meta['entries']:
        entry = album_meta['entries'][path]
        entry['caption'] = request.form['caption']
        entry['visibility_tags'] = request.form['visibility_tags'].split()
        entry['timezone'] = request.form['timezone']
        generate_timestamps(path, album_meta)
        layout_album(album_meta)
        write_album_meta(name, album_meta)
        try:
            if entry['timezone']:
                pytz.timezone(entry['timezone'])
        except pytz.exceptions.UnknownTimeZoneError:
            payload['success'] = False
            payload['message'] = 'Invalid timezone setting "%s". See the pytz documentation for valid settings.' % entry['timezone']
        return json.dumps(payload), 200, {'ContentType': 'application/json'}
    else:
        abort(404)

@app.route('/admin', methods=['GET'])
@login_required
def view_admin():
    albums = list_albums()
    album_parameters = []
    for album in albums:
        album_parameters.append({'name': album, 'meta': get_album_meta(album)})
    return render_template('admin.html', albums=album_parameters)

@app.route('/admin/album_meta', methods=['GET'])
@login_required
def view_admin_album_meta():
    album = request.args['name']
    album_parameters = {'name': album, 'meta': get_album_meta(album)}
    return render_template('admin_album_meta.html', album=album_parameters)

@app.route('/admin/album_meta_token', methods=['GET'])
@login_required
def view_admin_album_meta_token():
    album = request.args['album']
    token = request.args['token']
    album_parameters = {'name': album, 'meta': get_album_meta(album)}
    return render_template('admin_album_meta_token.html', album=album_parameters, token=token)

@app.route('/admin', methods=['POST'])
@login_required
def modify_album_meta():
    album = request.form['album']
    action = request.form['action']
    payload = {'success': True}
    if action == 'index':
        album_indexer = AlbumIndexer(album)
        album_indexer.start()
    elif action == 'create_token':
        album_meta = get_album_meta(album)
        token = secrets.token_urlsafe(ACESS_TOKEN_LENGTH)
        while token in album_meta['access_tokens']:
            token = secrets.token_urlsafe(ACESS_TOKEN_LENGTH)
        album_meta['access_tokens'][token] = ''
        write_album_meta(album, album_meta)
        payload['token'] = token
    elif action == 'save':
        album_meta = get_album_meta(album)
        album_meta['title'] = request.form['title']
        album_meta['timezone'] = request.form['timezone']
        for token in album_meta['access_tokens']:
            album_meta['access_tokens'][token] = request.form['tags_' + token].split()
        write_album_meta(album, album_meta)
        try:
            if album_meta['timezone']:
                pytz.timezone(album_meta['timezone'])
        except pytz.exceptions.UnknownTimeZoneError:
            payload['success'] = False
            payload['message'] = 'Invalid timezone setting "%s". See the pytz documentation for valid settings.' % album_meta['timezone']
    elif action == 'delete_token':
        album_meta = get_album_meta(album)
        token = request.form['token']
        del album_meta['access_tokens'][token]
        write_album_meta(album, album_meta)
    else:
        abort(404)
    return json.dumps(payload), 200, {'ContentType': 'application/json'} 

@app.route('/admin/progress', methods=['GET'])
@login_required
def view_admin_progress():
    def generator():
        try:
            global background_tasks
            global background_tasks_revision
            while True:
                current_revision = 0
                if background_tasks_revision != current_revision:
                    current_revision = background_tasks_revision
                    current_progress = []
                    for task in background_tasks:
                        current_progress.append({'album_name': task.name, 'progress': task.progress, 'work_units': task.work_units})
                    yield 'data:%s\n\n' % json.dumps(current_progress)
                time.sleep(2)
        except GeneratorExit:
            pass
    return Response(generator(), mimetype='text/event-stream')

# Media rendering
def render_media(filepath):
    pathext = os.path.splitext(filepath)[1].lower()
    def generator(path):
        with open(path, 'rb') as f:
             data = f.read(409600)
             while len(data) > 0:
                yield data
                data = f.read(409600)
    return Response(generator(filepath), mimetype=MIME_TYPES[pathext])

def render_album_media(name, path):
    albums = list_albums()
    album_meta = get_album_meta(name)
    if path in album_meta['entries']:
        filepath = os.path.join(albums[name], path)
        return render_media(filepath)
    abort(403)

def render_thumbnail(name, path):
    albums = list_albums()
    album_meta = get_album_meta(name)
    if path in album_meta['entries']:
        thumb_filename = os.path.splitext(path)[0] + '.jpg'
        filepath = os.path.join('thumb', name, thumb_filename)
        return render_media(filepath)
    abort(403)

# View media
@app.route('/view/<name>/<token>/<path:path>')
@token_required
def view_album_media(name, token, path):
    album_meta = get_album_meta(name)
    if token in album_meta['access_tokens']:
        tags = album_meta['access_tokens'][token]
        entry_tags = album_meta['entries'][path].get('visibility_tags', [])
        if contains_any(tags, entry_tags):
            return render_album_media(name, path)
    abort(403)

@app.route('/admin/view/<name>/<path:path>')
@login_required
def view_admin_album_media(name, path):
    return render_album_media(name, path)

# Thubnails
@app.route('/thumb/<name>/<token>/<path:path>')
@token_required
def view_thumbnail(name, token, path):
    album_meta = get_album_meta(name)
    if token in album_meta['access_tokens']:
        tags = album_meta['access_tokens'][token]
        entry_tags = album_meta['entries'][path].get('visibility_tags', [])
        if contains_any(tags, entry_tags):
            return render_thumbnail(name, path)
    abort(403)

@app.route('/admin/thumb/<name>/<path:path>')
@login_required
def view_admin_thumbnail(name, path):
    return render_thumbnail(name, path)