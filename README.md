# Album

Version 2.0

## Introduction

This is a web application for serving photo albums from your local storage.
No need to move files anywhere with simple tools for curating the presentation.

The motivation behind this project is to allow users to share photo albums with
family and friends without having to upload their sensitive pictures to a cloud
service. Another motivation is to allow filtering the content for different
viewers and provde comments and other adjustments to the presented media
so the user never has to otherwise tamper with their photos lying around on disk.

## Features

  * Can manage multiple albums using a simple admin page and JSON config file.
  * Handles both pictures and videos.
  * A simple viewer interface that can filter items based on date.
  * Items can be given a comment.
  * Timezone display can be overridden for a whole album and individual items.
  * Access tokens that can give different viewers access to different sets of images.

## Requirements

Python 3 with the flask and pytz modules.
Additionally the application uses ImageMagick and ffmpeg to generate thumbnails.
These programs need to be made available in the PATH variable.

## Installation

Set up a web server with a WSGI server that points to `app.wsgi` as entry point.
If using Apache with mod_wsgi under Windows you might want to modify `app.wsgi`
and set the variable `working_directory` to point to where you installed this
application.
You will also need to serve the static media under the static folder.

## Usage

Firstly the albums you want to be made available needs to be added under 'albums' in the
`config.json` file, for example:

```"albums": {
  "Family Trip 2014": "D:/Pictures/trip2014",
  "Atlantis 2016": "D:/Pictures/atlantis2016"
}```

You also need to set a `secret_key` and `admin_password` to values of your choosing.
This file needs to be edited manually since it paths into your local hard drives
as well as other secrets and must not be manipulated by any third party such as
the web application itself.

Start/restart the server and navigate to the `/admin` page and enter the admin
password you just set. On the admin page you should see the albums you just added
appear with an index button. Hit the index button and the application will start
generating thumbnails for your album and afterwards you will be presented with more
options for that album.

After indexing is done you can now set a title, change timezones, create access tokens
and view the album as an admin which will allow you to add comments, set visiblity
tags and change timezones for individual items.

Access tokens are used to create links for others to view your album.
They are associated with one or more visiblity tags which are separated by spaces.
Viewing a page using a certain access token means that all items without tags or
have at least one tag that is also in the list of tags for the access token will be displayed.
An access token without associated tags will display all items of the album.
