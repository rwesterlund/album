import sys
import os

# A workaround for mod_wsgi in Windows.
working_directory = ''

if working_directory:
	os.chdir(working_directory)
	sys.path.insert(0, working_directory)

from album import app as application

